/* 

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу. */

 const elemA = document.createElement('a');

 elemA.innerText = "Learn More";
 elemA.href = '#';

 const elemFooter = document.querySelector('footer');

 elemFooter.append(elemA);
 
 /*
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

 */

 const select = document.createElement('select');
 select.id = 'rating';

 for (var i=4; i>=1; i--)
{
    const option = document.createElement('option');
    option.value = i;
    option.innerText = `${i} Star` + (i>1 ? 's' : '');
    select.append(option);
    console.log(option);
}
 
 const main = document.querySelector('main');
 main.prepend(select);